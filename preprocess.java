import java.io.*;

public class preprocess{
	static File f = new File("./train_v2.csv");
	static File f2 = new File("./train_processed.csv");
	
	public static void main(String[] args) throws Exception{
		BufferedReader r = new BufferedReader(new FileReader(f));
		BufferedWriter w = new BufferedWriter(new FileWriter(f2));
		//r.readLine(); //ignore the first line
		w.write(r.readLine() + "\n");
		String temp = r.readLine();
		while(temp != null){
			w.write(pp(temp) + "\n");
			temp = r.readLine();
		}
		//r.readLine();
		r.close();
		w.close();
	}
	
	public static String pp(String line){
		String[] p = line.split(",");
		p[7] = p[7].split("\"")[1];
		p[8] = p[8].split("\"")[1];
		return concatenate(p);
	}
	
	public static String concatenate(String[] s){
		String rtn = s[0];
		for(int i=1; i<s.length; ++i){
			rtn += "," + s[i];
		}
		return rtn;
	}
}